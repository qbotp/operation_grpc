# Generated by the gRPC Python protocol compiler plugin. DO NOT EDIT!
import grpc
from grpc.framework.common import cardinality
from grpc.framework.interfaces.face import utilities as face_utilities

import operationservice.address_services_pb2 as address__services__pb2


class AddressmanagementStub(object):

  def __init__(self, channel):
    """Constructor.

    Args:
      channel: A grpc.Channel.
    """
    self.create_or_edit_address = channel.unary_unary(
        '/operationservices.Addressmanagement/create_or_edit_address',
        request_serializer=address__services__pb2.create_or_edit_address_request.SerializeToString,
        response_deserializer=address__services__pb2.create_or_edit_address_response.FromString,
        )
    self.retrieve_address = channel.unary_unary(
        '/operationservices.Addressmanagement/retrieve_address',
        request_serializer=address__services__pb2.address_retrieve_request.SerializeToString,
        response_deserializer=address__services__pb2.address_retrieve_response.FromString,
        )


class AddressmanagementServicer(object):

  def create_or_edit_address(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')

  def retrieve_address(self, request, context):
    context.set_code(grpc.StatusCode.UNIMPLEMENTED)
    context.set_details('Method not implemented!')
    raise NotImplementedError('Method not implemented!')


def add_AddressmanagementServicer_to_server(servicer, server):
  rpc_method_handlers = {
      'create_or_edit_address': grpc.unary_unary_rpc_method_handler(
          servicer.create_or_edit_address,
          request_deserializer=address__services__pb2.create_or_edit_address_request.FromString,
          response_serializer=address__services__pb2.create_or_edit_address_response.SerializeToString,
      ),
      'retrieve_address': grpc.unary_unary_rpc_method_handler(
          servicer.retrieve_address,
          request_deserializer=address__services__pb2.address_retrieve_request.FromString,
          response_serializer=address__services__pb2.address_retrieve_response.SerializeToString,
      ),
  }
  generic_handler = grpc.method_handlers_generic_handler(
      'operationservices.Addressmanagement', rpc_method_handlers)
  server.add_generic_rpc_handlers((generic_handler,))
